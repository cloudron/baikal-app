#!/bin/bash

set -eux

mkdir -p /app/data/Specific /run/baikal/sessions

if [[ -z "$(ls -A /app/data/Specific)" ]]; then
    echo "First run"

    echo "Order allow,deny\nDeny from all" > /app/data/Specific/.htaccess
    touch /app/data/Specific/INSTALL_DISABLED

    cp /app/code/config.php.template /app/data/Specific/config.php
    cp /app/code/config.system.php.template /app/data/Specific/config.system.php
fi

echo "Update config"
sed -e "s,##MYSQL_HOST,${MYSQL_HOST}," \
    -e "s,##MYSQL_PORT,${MYSQL_PORT}," \
    -e "s,##MYSQL_DATABASE,${MYSQL_DATABASE}," \
    -e "s,##MYSQL_USERNAME,${MYSQL_USERNAME}," \
    -e "s,##MYSQL_PASSWORD,${MYSQL_PASSWORD}," \
    -i /app/data/Specific/config.system.php

echo "Ensure folder ownership"
chown -R www-data.www-data /app/data /run/baikal

echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
